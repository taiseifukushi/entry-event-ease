# frozen_string_literal: true

class EventsController < ApplicationController
  before_action :set_all_events, only: %i[index show by_organizer]
  before_action :set_event, only: %i[show edit update destroy participants]
  before_action :check_owner, only: %i[edit update destroy]

  def index; end

  def show; end

  def new
    @event = Event.new
    @event.build_schedule
  end

  def create
    @event = Event.new
    if @event.create_related_resources(event_params, current_user)
      redirect_to @event, notice: "Event was successfully created."
    else
      render :new, alert: "Failed to create event."
    end
  end

  def edit; end

  def update
    if @event.update(event_params)
      redirect_to @event, notice: "Event was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    if @event.delete_checked_ownership_event
      redirect_to events_path, notice: "Event was successfully destroyed."
    else
      redirect_to @event, alert: "You are not allowed to delete this event."
    end
  end

  def by_organizer
    render :index
  end

  def participants
    @participants = @event.participants.page(params[:page])
    render "participants/index"
  end

  def join_event
    event = Event.find(params[:id])
    if !participant_registered?(event, current_user)
      participant = event.participants.new(user_id: current_user.id)
      if participant.save
        flash[:notice] = "Successfully joined the event."
      else
        flash[:alert] = "Failed to join the event."
      end
    else
      flash[:alert] = "You are already registered for this event."
    end
    redirect_to event_path(event)
  end

  def leave_event
    event = Event.find(params[:id])
    participant = Participant.find_by(event_id: event.id, user_id: current_user.id)
    if participant
      participant.destroy
      flash[:notice] = "Successfully left the event."
    else
      flash[:alert] = "You are not registered for this event."
    end
    redirect_to event_path(event)
  end

  private

  def set_all_events
    @organizers = Organizer.all
    if params[:organizer_id]
      organizer = Organizer.find(params[:organizer_id])
      @events = organizer.events.page(params[:page])
    else
      @events = Event.all.page(params[:page])
    end
  end

  def set_event
    @event = Event.find(params[:id])
  end

  def check_owner
    redirect_to @event, alert: "You are not authorized." unless @event.organizer.user_id == current_user.id
  end

  def event_params
    params.require(:event).permit(:title, :description, schedule_attributes: %i[start_time end_time])
  end
end
