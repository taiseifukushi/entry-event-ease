# frozen_string_literal: true

class OrganizersController < ApplicationController
  before_action :set_all_events, only: %i[index]

  def index; end

  private

  def set_all_events
    @organizers ||= Organizer.page(params[:page])
  end
end
