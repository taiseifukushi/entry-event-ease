# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def index; end

  def render_404
    render file: "#{Rails.root}/public/404.html", status: :not_found
  end

  def organizer?(event, user)
    event&.organizer&.user_id == user.id
  end
  helper_method :organizer?

  def participant_registered?(event, user)
    Participant.find_by(event_id: event.id, user_id: user.id).present?
  end
  helper_method :participant_registered?
end
