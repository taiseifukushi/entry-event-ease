# frozen_string_literal: true

class ParticipantsController < ApplicationController
  before_action :set_event

  def index
    @participants = @event.participants
  end

  private

  def set_event
    @event = Event.find(params[:event_id])
  end
end
