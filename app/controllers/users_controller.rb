# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update]
  before_action :authorize_user, only: %i[show edit update]

  def show; end

  def edit; end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: "User was successfully updated."
    else
      render :edit, alert: "Failed to update user."
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def authorize_user
    render_404 unless @user == current_user
  end

  def user_params
    params.require(:user).permit(:name, :email)
  end
end
