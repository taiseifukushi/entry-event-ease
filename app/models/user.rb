# frozen_string_literal: true

class User < ApplicationRecord
  # ===== devise =====
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  # ===== devise =====

  has_many :organizers
  has_many :events, through: :organizers
end
