# frozen_string_literal: true

class Event < ApplicationRecord
  belongs_to :organizer
  has_one :location
  has_one :category
  has_many :participants, dependent: :destroy
  has_one :schedule, dependent: :destroy
  accepts_nested_attributes_for :schedule, allow_destroy: true

  def create_related_resources(event_params, current_user)
    ActiveRecord::Base.transaction do
      organizer = Organizer.find_by(user_id: current_user.id) || Organizer.create!(user_id: current_user.id)
      update!(
        title: event_params[:title],
        description: event_params[:description],
        organizer_id: organizer.id
      )
    end
  rescue ActiveRecord::RecordInvalid, StandardError
    false
  end

  def delete_checked_ownership_event
    destroy!
  rescue StandardError
    false
  end
end
