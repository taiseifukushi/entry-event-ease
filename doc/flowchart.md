# Flowchart

```mermaid
graph TD
    UserRegistration[ユーザー登録]
    UserLogin[ログイン]
    EventCreation[イベント作成]
    EventParticipation1[イベント参加]
    CommentAddition[コメント追加]
    EventDisplay[イベント表示]
    CommentDisplay[コメント表示]
    EventSearch[イベント検索]
    UserLogout[ログアウト]
    EventParticipation2[イベント参加決定]

    UserRegistration --> UserLogin
    UserLogin --> EventCreation
    EventCreation -->|開催者として| EventDisplay
    UserLogin --> EventParticipation1
    EventParticipation1 -->|参加者として| EventDisplay
    EventDisplay --> EventParticipation2
    EventParticipation2 -->|参加者として| EventDisplay
    EventDisplay --> CommentAddition
    CommentAddition --> CommentDisplay
    CommentDisplay -->|コメント表示| EventDisplay
    UserLogin --> EventSearch
    EventSearch --> EventDisplay
    UserLogin --> UserLogout
```

```mermaid
graph TD
    UserRegistration[ユーザー: 参加者登録]
    EventRegistration[ユーザー: イベント参加登録]
    ParticipantCheck[サーバー: 参加者確認]
    IsParticipant{参加者か?}
    QRGeneration[サーバー: QRコード生成]
    QRCodeEmail[サーバー: QRコードメール送信]
    EventDay[参加者: イベント当日]
    QRReading[開催者: QRコード読み取り]
    ExpiryCheck{開催者: QRコード有効期限チェック}
    Entry[開催者: 入場許可]
    Denied[開催者: 入場拒否]

    UserRegistration --> EventRegistration
    EventRegistration --> ParticipantCheck
    ParticipantCheck --> IsParticipant
    IsParticipant -- Yes --> QRGeneration
    IsParticipant -- No --> Denied
    QRGeneration --> QRCodeEmail
    QRCodeEmail --> EventDay
    EventDay --> QRReading
    QRReading --> ExpiryCheck
    ExpiryCheck -->|有効| Entry
    ExpiryCheck -->|無効| Denied
```
