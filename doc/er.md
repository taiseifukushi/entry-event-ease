# ER図

```mermaid
erDiagram
    User ||--o{ Event : creates
    User ||--o{ Participant : "participates in"
    Event ||--o{ Participant : "has participants"
    User ||--|{ Organizer : organizes
    Event ||--|{ Organizer : "organized by"
    Event ||--|{ Location : "takes place at"
    Event ||--o{ Category : "belongs to"
    Event ||--|{ Schedule : "has schedule"
    Event ||--|{ Comment : "has comments"
    User ||--|{ Comment : "has comments"
    
    User {
        string name
        string email
        string password
    }
    Event {
        string title
        string description
    }
    Participant {
        references user FK
        references event FK
    }
    Organizer {
        references user FK
        references event FK
    }
    Location {
        references event FK
        string location
    }
    Category {
        references event FK
        string category "like tags"
    }
    Schedule {
        references event FK
        time startTime
        time endTime
    }
    Comment {
        references user FK
        references event FK    
        string text
    }
```
