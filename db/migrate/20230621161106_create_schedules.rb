# frozen_string_literal: true

class CreateSchedules < ActiveRecord::Migration[7.0]
  def change
    create_table :schedules do |t|
      t.references :event, foreign_key: true
      t.time :start_time
      t.time :end_time
      t.timestamps
    end
  end
end
