# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.string :title
      t.string :description
      t.references :organizer, foreign_key: { to_table: :users }
      t.references :location, foreign_key: true
      t.references :category, foreign_key: true
      t.timestamps
    end
  end
end
