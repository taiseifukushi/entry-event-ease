# frozen_string_literal: true

class CreateLocations < ActiveRecord::Migration[7.0]
  def change
    create_table :locations do |t|
      t.references :event, foreign_key: true
      t.string :location
      t.timestamps
    end
  end
end
