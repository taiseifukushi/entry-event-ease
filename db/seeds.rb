# frozen_string_literal: true

class Seeder
  EVENT_CREATE_COUNT = 30
  USER_CREATE_COUNT = 10
  CATEGORIES = %w[hobby study business].freeze
  LOCATIONS = %w[tokyo osaka hokakido].freeze

  def self.seed
    first_event = create_first_user_and_event
    create_categories_and_locations(first_event)
    create_users
    create_events
    update_categories_and_locations
  end

  def self.create_first_user_and_event
    user = User.create!(
      name: "First User",
      email: "#{DateTime.now.strftime('%Y%m%d%H%M%S')}@example.com",
      password: "password"
    )
    organizer = Organizer.create!(user_id: user.id)
    Event.create!(
      title: "First Event",
      description: "This is a test event.",
      organizer_id: organizer.id
    )
  end

  def self.create_categories_and_locations(first_event)
    CATEGORIES.each { |c| Category.create!(category: c, event_id: first_event.id) }
    LOCATIONS.each { |l| Location.create!(location: l, event_id: first_event.id) }
  end

  def self.create_users
    USER_CREATE_COUNT.times do |i|
      User.create!(
        name: "Test User #{i + 1}",
        email: "test#{i + 1}@example.com",
        password: "password"
      )
    end
  end

  def self.create_events
    EVENT_CREATE_COUNT.times do |i|
      users = User.all
      organizers = Organizer.all

      event = Event.create!(
        title: "Test Event #{i + 1}_#{DateTime.now.strftime('%Y%m%d%H%M%S')}",
        description: "This is a test event.",
        organizer_id: organizers.sample.id,
        category_id: Category.all.sample.id,
        location_id: Location.all.sample.id
      )

      random = rand(1..5)
      users = User.all.sample(random).uniq
      users.each do |u|
        Participant.create!(
          user_id: u.id,
          event_id: event.id
        )
      end

      Schedule.create!(
        event_id: event.id,
        start_time: Time.now,
        end_time: Time.now + rand(1..5).hour
      )

      Comment.create!(
        user_id: users.sample.id,
        event_id: event.id,
        text: "This is a test comment."
      )
    end
  end

  def self.update_categories_and_locations
    events = Event.all
    CATEGORIES.each { |_c| Category.update!(event_id: events.sample.id) }
    LOCATIONS.each { |_l| Location.update!(event_id: events.sample.id) }
  end
end

Seeder.seed
