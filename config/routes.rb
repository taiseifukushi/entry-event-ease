# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    get "/users/sign_out" => "devise/sessions#destroy"
  end

  root "home#index"
  get "/navigations", to: "navigations#index"

  resources :events do
    get "participants", on: :member
    member do
      post "join_event"
      delete "leave_event"
    end
  end

  resources :organizers, only: [:index]
  get "organizers/:organizer_id/events", to: "events#by_organizer", as: "organizer_events"
  resources :users, only: %i[show edit update]

  resources :participants, only: [:index]

  get "/categories", to: "categories#index"
  get "/comments", to: "comments#index"
  get "/locations", to: "locations#index"
  get "/schedules", to: "schedules#index"
end
